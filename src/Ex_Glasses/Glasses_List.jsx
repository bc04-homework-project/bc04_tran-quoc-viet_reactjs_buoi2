import React, { Component } from "react";
// import { dataGlasses } from "./dataGlasses";

export default class Glasses_List extends Component {
  render() {
    return (
      <div>
        {this.props.dataGlasses.map((glasses, index) => {
          return (
            <img
              key={index.toString() + glasses.id}
              onClick={() => {
                this.props.handleChangeGlasses(index);
              }}
              className="glasses_img m-2"
              src={glasses.url}
            />
          );
        })}
      </div>
    );
  }
}
