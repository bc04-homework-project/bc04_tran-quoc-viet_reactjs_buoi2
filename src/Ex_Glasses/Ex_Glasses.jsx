import React, { Component } from "react";
import Glasses_List from "./Glasses_List";
import { dataGlasses } from "./dataGlasses";

export default class Ex_Glasses extends Component {
  state = {
    urlGlasses: "",
    glassesName: '',
    glassesInfo: '',
    glassesPrice: '',
  };

  handleChangeGlasses = (index) => {
    this.setState({
      urlGlasses: dataGlasses[index].url,
      glassesName: dataGlasses[index].name,
      glassesInfo: dataGlasses[index].desc,
      glassesPrice: dataGlasses[index].price,
    });
  };

  handleRemoveGlasses = () => {
    this.setState({
      urlGlasses: '',
    });
  }

  render() {
    return (
      <div className="ex_glasses">
        <div className="head_title">
          <p>TRY GLASSES APP ONLINE</p>
        </div>

        <div className="container p-5">
          <div className="container model_glasses">
            <img className="vGlasses" src={this.state.urlGlasses} onClick={this.handleRemoveGlasses} />
            <div className="glasses_info">
              <h4>{this.state.glassesName}</h4>
              <button className="btn btn-success">{this.state.glassesPrice}</button>
              <h6>{this.state.glassesInfo}</h6>
            </div>
          </div>

          <div className="container glasses py-3 my-md-5 my-3">
            <Glasses_List
              handleChangeGlasses={this.handleChangeGlasses}
              dataGlasses={dataGlasses}
            />
          </div>
        </div>
      </div>
    );
  }
}
